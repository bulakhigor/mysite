let smartgrid = require('smart-grid');

/* It's principal settings in smart grid project */
let settings = {
    outputStyle: 'less', /* less || scss || sass || styl */
    columns: 12, /* number of grid columns */
    offset: '30px', /* gutter width px || % */
    mobileFirst: false, /* mobileFirst ? 'min-width' : 'max-width' */
    container: {
        maxWidth: '1400px', /* max-width оn very large screen */
        fields: '115px' /* side fields */
    },
    breakPoints: {
        lg: {
            width: '1290px', /* -> @media (max-width: 1100px) */
        },
        md: {
            width: '960px'
        },
        sm: {
            width: '780px',
            fields: '15px' /* set fields only if you want to change container.fields */
        },
        xs: {
            width: '560px'
        },
        xxs: {
            width: '320px'
        }
    }
};

smartgrid('./app/less', settings);