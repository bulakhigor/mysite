let gulp = require('gulp');
let autoprefixer = require('gulp-autoprefixer');
let clean = require('gulp-clean-css');
let gcmq = require('gulp-group-css-media-queries');
let less = require('gulp-less');
let browserSync = require('browser-sync').create();
let rename = require('gulp-rename');
let concat = require('gulp-concat');
let del = require('del');
let uglify = require('gulp-uglify');
let sourcemaps = require('gulp-sourcemaps');
let babel = require('gulp-babel');
//let imagemin = require('gulp-imagemin');

let config = {
    src: './app',
    build: './build',
    html: {
        src: '/*.html',
        dest: '/'
    },
    fonts: {
        src: '/fonts/*',
        dest: '/fonts/'
    },
    js: {
        src: '/js/*',
        dest: '/js/'
    },
    img: {
        src: '/img/*',
        dest: '/img/'
    },
    css: {
        src: '/css/*',
        dest: '/css/'
    },
    less: {
        watch: '/less/**/*.less',
        src: '/less/styles.less',
        dest: '/css/'
    }
};

gulp.task('html', function () {
    gulp.src(config.src + config.html.src)
        .pipe(gulp.dest(config.build + config.html.dest))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('fonts', function () {
    gulp.src(config.src + config.fonts.src)
        .pipe(gulp.dest(config.build + config.fonts.dest));
});

gulp.task('img', function () {
    gulp.src(config.src + config.img.src)
        /*.pipe(imagemin({
            progressive: true
        }))*/
        .pipe(gulp.dest(config.build + config.img.dest));
});

gulp.task('js', function () {
    gulp.src(config.src + config.js.src)
        .pipe(sourcemaps.init())
        .pipe(concat('all.js'))
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(uglify())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.build + config.js.dest))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('css', function () {
    gulp.src(config.src + config.css.src)
        .pipe(sourcemaps.init())
        .pipe(clean({
            level: 2
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.build + config.css.dest))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('del', function () {
    let path = config.build + '/*';

    if(path.substr(0, 1) === '/'){
        console.log("Never delete files from root!");
    }else{
        del.sync(path);
    }
});

gulp.task('less', function () {
    gulp.src(config.src + config.less.src)
        .pipe(less())
        .pipe(gcmq())
        .pipe(sourcemaps.init())
        .pipe(autoprefixer({
            browsers: ['> 0.01%'],
            cascade: false
        }))
        .pipe(clean({
            level: 2
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(config.build + config.less.dest))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('browserSync', function () {
    browserSync.init(({
        server: {
            baseDir: config.build
        }
    }))
});

gulp.task('watch',['browserSync'], function () {
    gulp.watch(config.src + config.less.watch, ['less']);
    gulp.watch(config.src + config.html.src, ['html']);
    gulp.watch(config.src + config.js.src, ['js']);
});

gulp.task('all', ['del', 'html', 'fonts', 'img', 'js', 'css', 'less'], function () {

});